#include <Arduino.h>
#include <TeleInfo.h>
#include <unity.h>
#include "helpers.h"

// void setUp(void) {
// // set stuff up here
// }

// void tearDown(void) {
// // clean stuff up here
// }

String output;
StringStream stream(output);
TeleInfo teleinfo(&stream);

void test_get_teleinfo(void) {
  teleinfo.process();
  
  TEST_ASSERT_EQUAL(true, teleinfo.available());
  teleinfo.resetAvailable();
}

void setup() {
  // NOTE!!! Wait for >2 secs
  // if board doesn't support software reset via Serial.DTR/RTS
  delay(2000);
  teleinfo.begin();
  UNITY_BEGIN();    // IMPORTANT LINE!
}

void loop() {
  RUN_TEST(test_get_teleinfo);
  UNITY_END(); // stop unit testing
}